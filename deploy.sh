kubectl delete configmap mockserver | true
kubectl create configmap mockserver --from-file=configmaps/ -n ${NAMESPACE}
kubectl apply -f deployment.yml -n ${NAMESPACE}
kubectl apply -f service.yml -n ${NAMESPACE}
kubectl apply -f mapping.yml -n ${NAMESPACE}
