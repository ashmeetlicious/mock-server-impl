# README #

* Quick summary
The project contains mockserver setup to be deployed on kubernetes.
Learn more about mockserver https://www.mock-server.com/

### How do I get set up? ###

Deploying Mockserver
1. Replace <external_url> in mapping.yml with host url you would like to use to access the mockserver from outside the kubernetes cluster
2. chmod a+x deploy.sh
3. NAMESPACE=<namespace> ./deploy.sh

To Do (Improvements):
1. The mockserver dashboard is not working due to some error in websocket connection. this needs to be resolved
2. Use some templating system to manage variables like mapping host etc
### Who do I talk to? ###

* rahulyadav@licious.com
